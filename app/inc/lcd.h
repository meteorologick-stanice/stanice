#include "stm8s.h"

//delay
void delay(void);
// E vypnout a zapnout (pro poslání 8-bitového kodu)
void EON(void);
void EOFF(void);




//RW 1/0
void startwrite(void);
void endwrite(void);


//posun kurzoru
void posunkurzoru(uint8_t smer);



// číslice z asci tabulky
void writecislo(int16_t a);




//písmena z asci tabulky
void writeznak(char b);

