#include "lcd.h"

void delay(void)
{
    for(uint32_t i = 0;i<100000;i++);
}

void EON(void)
{
  GPIO_WriteHigh(GPIOC,GPIO_PIN_6);


}
void EOFF(void)
{
  GPIO_WriteLow(GPIOC,GPIO_PIN_6);


}


void posunkurzoru(uint8_t smer){
  if (smer == 1){   //doprava
    GPIO_Write(GPIOD, 0b00010100);
    delay();
    EON();
    EOFF();
  }
  else if (smer == 0){  //doleva
    GPIO_Write(GPIOD, 0b00010000);
    delay();
    EON();
    EOFF();

  }


}





void startwrite(void)
{
  GPIO_WriteHigh(GPIOC,GPIO_PIN_7);
  
}
void endwrite(void)
{
  
  GPIO_WriteLow(GPIOC,GPIO_PIN_7);

}


// inicialzace čísel

void writecislo(int16_t a)
{
if (a == 0){

  GPIO_Write(GPIOD, 0b00110000);
  delay();
  EON();
  EOFF();
  delay();
}
else if (a==1){
  GPIO_Write(GPIOD, 0b00110001);
  delay();
  EON();
  EOFF();
  delay();
}
else if (a==2){
  GPIO_Write(GPIOD, 0b00110010);
  delay();
  EON();
  EOFF();
  delay();

}
else if (a==3){
  GPIO_Write(GPIOD, 0b00110011);
  delay();
  EON();
  EOFF();
  delay();
}
else if (a==4){
  GPIO_Write(GPIOD, 0b00110100);
  delay();
  EON();
  EOFF();
  delay();

}
else if (a==5){
  GPIO_Write(GPIOD, 0b00110101);
  delay();
  EON();
  EOFF();
  delay();
}
else if (a==6){
  GPIO_Write(GPIOD, 0b00110110);
  delay();
  EON();
  EOFF();
  delay();
}
else if (a==7){
  GPIO_Write(GPIOD, 0b00110111);
  delay();
  EON();
  EOFF();
  delay();

}
else if (a==8){
  GPIO_Write(GPIOD, 0b00111000);
  delay();
  EON();
  EOFF();
  delay();
}
else if (a==9){
  GPIO_Write(GPIOD, 0b00111001);
  delay();
  EON();
  EOFF();
  delay();

}
}



void writeznak(char b)
{ 
if (b == 'A'){
  GPIO_Write(GPIOD, 0b01000001);
  delay();
  EON();
  EOFF();
  delay();
  
}
else if (b == 'a'){
  GPIO_Write(GPIOD, 0b01100001);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'B'){
  GPIO_Write(GPIOD, 0b01000010);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'b'){
  GPIO_Write(GPIOD, 0b01100010);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'C'){
  GPIO_Write(GPIOD, 0b01000011);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'c'){
  GPIO_Write(GPIOD, 0b01100011);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'D'){
  GPIO_Write(GPIOD, 0b01000100);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'd'){
  GPIO_Write(GPIOD, 0b01100100);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'E'){
  GPIO_Write(GPIOD, 0b01000101);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'e'){
  GPIO_Write(GPIOD, 0b01100101);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'F'){
  GPIO_Write(GPIOD, 0b01000110);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'f'){
  GPIO_Write(GPIOD, 0b01100110);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'G'){
  GPIO_Write(GPIOD, 0b01000111);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'g'){
  GPIO_Write(GPIOD, 0b01100111);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'H'){
  GPIO_Write(GPIOD, 0b01001000);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'h'){
  GPIO_Write(GPIOD, 0b01101000);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'I'){
  GPIO_Write(GPIOD, 0b01001001);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'i'){
  GPIO_Write(GPIOD, 0b01101001);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'J'){
  GPIO_Write(GPIOD, 0b01001010);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'j'){
  GPIO_Write(GPIOD, 0b01101010);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'K'){
  GPIO_Write(GPIOD, 0b01001011);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'k'){
  GPIO_Write(GPIOD, 0b01101011);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'L'){
  GPIO_Write(GPIOD, 0b01001100);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'l'){
  GPIO_Write(GPIOD, 0b01101100);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'M'){
  GPIO_Write(GPIOD, 0b01001101);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'm'){
  GPIO_Write(GPIOD, 0b01101101);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'N'){
  GPIO_Write(GPIOD, 0b01001110);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'n'){
  GPIO_Write(GPIOD, 0b01101110);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'O'){
  GPIO_Write(GPIOD, 0b01001111);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'o'){
  GPIO_Write(GPIOD, 0b01101111);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'P'){
  GPIO_Write(GPIOD, 0b01010000);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'p'){
  GPIO_Write(GPIOD, 0b01110000);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'Q'){
  GPIO_Write(GPIOD, 0b01010001);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'q'){
  GPIO_Write(GPIOD, 0b01110001);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'R'){
  GPIO_Write(GPIOD, 0b01010010);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'r'){
  GPIO_Write(GPIOD, 0b01110010);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'S'){
  GPIO_Write(GPIOD, 0b01010011);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 's'){
  GPIO_Write(GPIOD, 0b01110011);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'T'){
  GPIO_Write(GPIOD, 0b01010100);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 't'){
  GPIO_Write(GPIOD, 0b01110100);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'U'){
  GPIO_Write(GPIOD, 0b01010101);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'u'){
  GPIO_Write(GPIOD, 0b01110101);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'V'){
  GPIO_Write(GPIOD, 0b01010110);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'v'){
  GPIO_Write(GPIOD, 0b01110110);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'W'){
  GPIO_Write(GPIOD, 0b01010111);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'w'){
  GPIO_Write(GPIOD, 0b01110111);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'X'){
  GPIO_Write(GPIOD, 0b01011000);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'x'){
  GPIO_Write(GPIOD, 0b01111000);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'Y'){
  GPIO_Write(GPIOD, 0b01011001);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'y'){
  GPIO_Write(GPIOD, 0b01111001);
  delay();
  EON();
  EOFF();
  delay();
}
if (b == 'Z'){
  GPIO_Write(GPIOD, 0b01011010);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == 'z'){
  GPIO_Write(GPIOD, 0b01111010);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == ':'){
  GPIO_Write(GPIOD, 0b00111010);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == '.'){
  GPIO_Write(GPIOD, 0b00101110);
  delay();
  EON();
  EOFF();
  delay();
}
else if (b == '§'){
  GPIO_Write(GPIOD, 0b11011111);
  delay();
  EON();
  EOFF();
  delay();
}
}





