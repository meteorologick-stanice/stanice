#include "stm8s.h"
#include "lcd.h"
#include <math.h>




void main(void)
{
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
    
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW); // LED
    GPIO_Init(GPIOB, GPIO_PIN_0, GPIO_MODE_IN_FL_NO_IT);     // ADC CHANNEL 0 -> PCB LABEL A5
    GPIO_Init(GPIOB, GPIO_PIN_1, GPIO_MODE_IN_FL_NO_IT);     //CHANNEL 1

TIM4_TimeBaseInit(TIM4_PRESCALER_128, 62499);
TIM4_Cmd(ENABLE);
GPIO_Init(GPIOD, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_FAST); //data pin 0   
GPIO_Init(GPIOD, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_FAST); //data pin 1
GPIO_Init(GPIOD, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_FAST); //data pin 2
GPIO_Init(GPIOD, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_FAST); //data pin 3
GPIO_Init(GPIOD, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_FAST); //data pin 4
GPIO_Init(GPIOD, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_FAST); //data pin 5
GPIO_Init(GPIOD, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_FAST); //data pin 6
GPIO_Init(GPIOD, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_FAST); //data pin 7
GPIO_Init(GPIOC, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_FAST); //rs pin
GPIO_Init(GPIOC, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_FAST); //enable pin
GPIO_Init(GPIOC, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_FAST); //rs pin
int16_t d1 = 0;
int16_t d2 = 0;
int16_t d3 = 0;
int16_t d4 = 0;
float R = 0; //R proměná, která uchovává hodnotu termistoru
float T = 0.0; //proměná pro teplotu
GPIO_Write(GPIOD, 0b00000001);  //clear display
delay();
EON();
EOFF();
delay();
GPIO_Write(GPIOD, 0b00000010); //set cursor to home position
delay();
EON();
EOFF();
delay();
GPIO_Write(GPIOD, 0b00001100); //display on/cursor off without flashing
delay();
EON();
EOFF();
delay();
GPIO_Write(GPIOD, 0b00111100); //set display to 16/2 mode
delay();
EON();
EOFF();
delay();
startwrite();
delay();
GPIO_Write(GPIOD, 0b00000000);//clear bite
delay();
writeznak('I');
writeznak('n');
writeznak('d');
writeznak('o');
writeznak('o');
writeznak('r');
writeznak(':');
endwrite();
while(1){
        ADC2_Init(                //adc pro první termistor
        ADC2_CONVERSIONMODE_CONTINUOUS,
        ADC2_CHANNEL_0,
        ADC2_PRESSEL_FCPU_D18,
        ADC2_EXTTRIG_GPIO,
        DISABLE,
        ADC2_ALIGN_RIGHT,
        ADC2_SCHMITTTRIG_CHANNEL0,
        DISABLE
    );
    ADC2_Cmd(ENABLE);
        ADC2_StartConversion();
        while (!ADC2_GetFlagStatus())
            ;

        uint16_t adc_value = ADC2_GetConversionValue();
        ADC2_ClearFlag();
        uint16_t ADC_voltage = adc_value * (3.226); //(3300/1023 =~ 3.226)convert ADC value 1 to 0 to 3300mV
        R = 100000*((3.3/(ADC_voltage/1000.0))-1); // výpočet termistoru pomocí děliče
        T = 1.0/((1.0/298.15)+(1.0/3977.0)*(logf(R/100000.0))); //výpočet pomocí Steinhartova-Hartova vztahu
        T = T-273.15; //převod z kelvina na Celsius 
        T = T*100;  
        T = T/1;  
        d4 = (T/1000);  
        d3 = (T - (d4*1000))/100;
        d2 = (T - (d4*1000)-(d3*100))/10;
        d1 = (T - (d4*1000)-(d3*100)-(d2*10));
        startwrite(); //RS na HIGH pro začátek psaní 
        writecislo(d4);
        writecislo(d3);
        writeznak('.');
        writecislo(d2);
        writecislo(d1);
        writeznak('§'); //znak celsius
        writeznak('C');
        delay();
        endwrite();
        for (uint32_t i = 0;i<26;i++){ //posun kurzoru na dolní pozice displeje
            posunkurzoru(1); //posun doprava
        }
        startwrite();
        writeznak('O');
        writeznak('u');
        writeznak('t');
        writeznak('d');
        writeznak('o');
        writeznak('o');
        writeznak('r');
        writeznak(':');
        ADC2_DeInit(); //ADC pro termitor 10K
        ADC2_Init(
        ADC2_CONVERSIONMODE_CONTINUOUS,
        ADC2_CHANNEL_1,
        ADC2_PRESSEL_FCPU_D18,
        ADC2_EXTTRIG_GPIO,
        DISABLE,
        ADC2_ALIGN_RIGHT,
        ADC2_SCHMITTTRIG_CHANNEL1,
        DISABLE
    );
    ADC2_Cmd(ENABLE);
    ADC2_StartConversion();
        while (!ADC2_GetFlagStatus())
            ;

        uint16_t adc2_value = ADC2_GetConversionValue();
        ADC2_ClearFlag();
        uint16_t ADC2_voltage = adc2_value * (3.226); //(3300/1023 =~ 3.226)convert ADC value 1 to 0 to 3300mV
        R = 10000*((3.3/(ADC2_voltage/1000.0))-1);
        T = 1.0/((1.0/298.15)+(1.0/3977.0)*(logf(R/10000.0)));
        T = T-273.15;
        T = T*100;
        T = T/1;
        d4 = (T/1000);
        d3 = (T - (d4*1000))/100;
        d2 = (T - (d4*1000)-(d3*100))/10;
        d1 = (T - (d4*1000)-(d3*100)-(d2*10));
        ADC2_DeInit();
        writecislo(d4);
        writecislo(d3);
        writeznak('.');
        writecislo(d2);
        writecislo(d1);
        writeznak('§');
        writeznak('C');
        endwrite();
        GPIO_Write(GPIOD, 0b00000010); // kurzor na domovskou pozici 
        delay();
        EON();
        delay();
        EOFF();
        for (uint32_t x = 0;x<7;x++){
            posunkurzoru(1);
        }
        for(uint32_t p = 0;p<100000;p++){ //15 sec delay
           while (TIM4_GetFlagStatus(TIM4_FLAG_UPDATE) != SET)
               ;
           TIM4_ClearFlag(TIM4_FLAG_UPDATE);
        }
        }
}
