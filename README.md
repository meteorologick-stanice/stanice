# METEOROLOGICKÁ STANICE


## Úvod

Pokusil jsem se vytvořit meteorologickou stanici, která bude zobrazovat čas, teplotu venku a teplotu vevnitř. Teplota je měřena pomocí dvou thermistorů. Vše se zobrazuje na 16x2 LCD displeji. Bohužel čas meteostanice nezobrazuje a to z toho důvodu, že jsem nepřišel na to, jak efektivně měřit aktuální čas a přijde mi zbytečné používat k měření timery, které nejsou dostatečně přesné

![stanice](img/stanice.jpg)


## Blokové schéma

```mermaid
flowchart LR
PC ==> STM8[STM8]
STM8 ==> LCD
ADC2 <--> STM8
Termistor1 ==> ADC2
Termistor2 ==> ADC2
```

## Schéma
![Schéma](img/schema.jpg)

## Vývojový diagram
![diagram](img/diagram.jpg)

## Tabulka součástek

|   Typ součástky |      Počet kusů      |  Cena/1 ks  | Cena  |
|:---------------:|:--------------------:|:-----------:|:-----:|
|Termistor 10k    |    1                 | 20 Kč       | 25 Kč |
|Termistor 100K   |    1                 | 20 Kč       | 9  Kč |
| STM8 Nucleo     |    1                 | 250 Kč      | 250 Kč|
|LCD              |    1                 |  96 Kč      | 96 Kč |

## Odkaz na součástky

- [STM8](https://www.st.com/en/evaluation-tools/nucleo-8s208rb.html)
- [LCD 16x2](https://dratek.cz/arduino/836-display-modry-16x2-znaku.html?gclid=CjwKCAjw77WVBhBuEiwAJ-YoJDR87wQPlJd7DUj25QPfV0p2svOGkJslsbFQTgCBpTgvPIWBgKe1PhoCtygQAvD_BwE)
- [Termistor 100k](https://www.laskakit.cz/termistor-ntc-mf5b-100k-1/)
- [Termistor 10K](https://www.laskakit.cz/vodotesne-cidlo-teploty-2m-ntc-termistor-10k-1--3950/)

## Součástky

## LCD 16x2




LCD displej funguje na principu posílání 8, popřípadě 4 bitů(ve čtyř bitovém módu), displej je nejdříve nutno nakonfigurovat pomocí předem určeného datového vstupu viz [DatasheetLCD](https://www.sparkfun.com/datasheets/LCD/ADM1602K-NSW-FBS-3.3v.pdf). V mém případě byl použit osmi-bitový mód, který vyžaduje poloviční počet čtení bitů tzv(místo toho abych dvakrát poslal 4-bitové data stačí mi poslat jednou 8-bitové data). Dále musíme zvolit zda chceme používat pouze jeden řádek displeje a efektivně tak udělat 16x1 displej, nebo chceme používat oba dva řádky. Na druhý displej se dostaneme v případě, že se kurzor posune na čtyřicáte místo v prvním řádku a zpátky na horní část jej můžeme přivést pomocí bin. kódu - (00000010) Jakmile máme všechno nakonfigurované stačí přepnout R/W do úrovně HIGH a potom již stačí poslat binární kód dle ASCII tabulky nebo tabulky v datasheetu 
![LCD](img/lcd.jpg)


## termistory




V mém projektu jsem použil NTC 100k a NTC10K termistor, což znamená, že s narůstající teplotou klesá odpor a je více citlivý na změny teplot. Závislost odporu na teplotě udává ![Steinhartův vztah](img/rovnice.img.jpg)
![termistor100](img/termistor1.jpg)
![termistor10](img/termistor2.jpg)
