EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM8:STM8S208RB U1
U 1 1 62AE06EF
P 7950 3700
F 0 "U1" H 7950 5681 50  0000 C CNN
F 1 "STM8S208RB" H 7950 5590 50  0000 C CNN
F 2 "Package_QFP:LQFP-64_10x10mm_P0.5mm" H 8000 1500 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/stm8s208rb.pdf" H 7850 3700 50  0001 C CNN
	1    7950 3700
	1    0    0    -1  
$EndComp
Text HLabel 8700 2100 2    50   Output ~ 0
PD0
Wire Wire Line
	8550 2100 8700 2100
Text HLabel 8700 2200 2    50   Output ~ 0
PD1
Wire Wire Line
	8550 2200 8700 2200
Text HLabel 8700 2300 2    50   Output ~ 0
PD2
Wire Wire Line
	8550 2300 8700 2300
Text HLabel 8700 2400 2    50   Output ~ 0
PD3
Wire Wire Line
	8550 2400 8700 2400
Text HLabel 8700 2500 2    50   Output ~ 0
PD4
Wire Wire Line
	8550 2500 8700 2500
Text HLabel 8700 2600 2    50   Output ~ 0
PD5
Wire Wire Line
	8550 2600 8700 2600
Text HLabel 8700 2700 2    50   Output ~ 0
PD6
Wire Wire Line
	8550 2700 8700 2700
Text HLabel 8700 2800 2    50   Output ~ 0
PD7
Wire Wire Line
	8550 2800 8700 2800
Text HLabel 7200 4200 0    50   Output ~ 0
PC6
Wire Wire Line
	7350 4200 7200 4200
Text HLabel 7200 4300 0    50   Output ~ 0
PC7
Wire Wire Line
	7350 4300 7200 4300
Text HLabel 7200 2800 0    50   Input ~ 0
PB0
Wire Wire Line
	7350 2800 7200 2800
Text HLabel 7200 2900 0    50   Input ~ 0
PB1
Wire Wire Line
	7350 2900 7200 2900
$Comp
L power:+5V #PWR?
U 1 1 62AE758D
P 7750 1650
F 0 "#PWR?" H 7750 1500 50  0001 C CNN
F 1 "+5V" H 7765 1823 50  0000 C CNN
F 2 "" H 7750 1650 50  0001 C CNN
F 3 "" H 7750 1650 50  0001 C CNN
	1    7750 1650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62AEBCFB
P 8050 1650
F 0 "#PWR?" H 8050 1400 50  0001 C CNN
F 1 "GND" H 8055 1477 50  0000 C CNN
F 2 "" H 8050 1650 50  0001 C CNN
F 3 "" H 8050 1650 50  0001 C CNN
	1    8050 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	7750 1900 7750 1650
Wire Wire Line
	7850 1900 7850 1650
Wire Wire Line
	8050 1650 7850 1650
$Comp
L Device:R R3
U 1 1 62AEE926
P 7000 1900
F 0 "R3" V 6793 1900 50  0000 C CNN
F 1 "0R" V 6884 1900 50  0000 C CNN
F 2 "" V 6930 1900 50  0001 C CNN
F 3 "~" H 7000 1900 50  0001 C CNN
	1    7000 1900
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 62AEF512
P 7000 2200
F 0 "R4" V 6793 2200 50  0000 C CNN
F 1 "0R" V 6884 2200 50  0000 C CNN
F 2 "" V 6930 2200 50  0001 C CNN
F 3 "~" H 7000 2200 50  0001 C CNN
	1    7000 2200
	0    1    1    0   
$EndComp
$Comp
L Device:Crystal X2
U 1 1 62AEF74F
P 6700 2050
F 0 "X2" V 6746 1919 50  0000 R CNN
F 1 "8MHz" V 6655 1919 50  0000 R CNN
F 2 "" H 6700 2050 50  0001 C CNN
F 3 "~" H 6700 2050 50  0001 C CNN
	1    6700 2050
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C3
U 1 1 62AF02EB
P 6450 1900
F 0 "C3" V 6198 1900 50  0000 C CNN
F 1 "12pF" V 6289 1900 50  0000 C CNN
F 2 "" H 6488 1750 50  0001 C CNN
F 3 "~" H 6450 1900 50  0001 C CNN
	1    6450 1900
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 62AF0E7B
P 6450 2200
F 0 "C?" V 6198 2200 50  0000 C CNN
F 1 "12pF" V 6289 2200 50  0000 C CNN
F 2 "" H 6488 2050 50  0001 C CNN
F 3 "~" H 6450 2200 50  0001 C CNN
	1    6450 2200
	0    1    1    0   
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 62AF2062
P 6200 2200
F 0 "#PWR?" H 6200 1950 50  0001 C CNN
F 1 "GNDREF" H 6205 2027 50  0000 C CNN
F 2 "" H 6200 2200 50  0001 C CNN
F 3 "" H 6200 2200 50  0001 C CNN
	1    6200 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 62AF291D
P 6200 1900
F 0 "#PWR?" H 6200 1650 50  0001 C CNN
F 1 "GNDREF" H 6205 1727 50  0000 C CNN
F 2 "" H 6200 1900 50  0001 C CNN
F 3 "" H 6200 1900 50  0001 C CNN
	1    6200 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 2100 7250 2100
Wire Wire Line
	7250 2100 7250 1900
Wire Wire Line
	7250 1900 7150 1900
Wire Wire Line
	6850 1900 6700 1900
Connection ~ 6700 1900
Wire Wire Line
	6700 1900 6600 1900
Wire Wire Line
	7350 2200 7150 2200
Wire Wire Line
	6850 2200 6700 2200
Wire Wire Line
	6700 2200 6600 2200
Connection ~ 6700 2200
Wire Wire Line
	6300 1900 6200 1900
Wire Wire Line
	6300 2200 6200 2200
$Comp
L Device:C C1
U 1 1 62AF75A0
P 6450 5450
F 0 "C1" H 6565 5496 50  0000 L CNN
F 1 "100nF" H 6565 5405 50  0000 L CNN
F 2 "" H 6488 5300 50  0001 C CNN
F 3 "~" H 6450 5450 50  0001 C CNN
	1    6450 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 62AF81A0
P 6750 5700
F 0 "R1" V 6543 5700 50  0000 C CNN
F 1 "100R" V 6634 5700 50  0000 C CNN
F 2 "" V 6680 5700 50  0001 C CNN
F 3 "~" H 6750 5700 50  0001 C CNN
	1    6750 5700
	0    1    1    0   
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 62AF8C77
P 7050 5800
F 0 "#PWR?" H 7050 5550 50  0001 C CNN
F 1 "GNDREF" H 7055 5627 50  0000 C CNN
F 2 "" H 7050 5800 50  0001 C CNN
F 3 "" H 7050 5800 50  0001 C CNN
	1    7050 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 5600 6450 5700
Wire Wire Line
	6450 5700 6600 5700
Wire Wire Line
	6900 5700 7050 5700
Wire Wire Line
	7050 5800 7050 5700
$Comp
L Switch:SW_Push RESET
U 1 1 62B02328
P 7050 5500
F 0 "RESET" H 7050 5785 50  0000 C CNN
F 1 "BUTTON 2" H 7050 5694 50  0000 C CNN
F 2 "" H 7050 5700 50  0001 C CNN
F 3 "~" H 7050 5700 50  0001 C CNN
	1    7050 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 5300 7050 5300
Connection ~ 7050 5300
Wire Wire Line
	7050 5300 7350 5300
Connection ~ 7050 5700
$Comp
L Device:C C4
U 1 1 62B09D24
P 9800 3550
F 0 "C4" H 9685 3596 50  0000 R CNN
F 1 "100nF" H 9685 3505 50  0000 R CNN
F 2 "" H 9838 3400 50  0001 C CNN
F 3 "~" H 9800 3550 50  0001 C CNN
	1    9800 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9800 3700 9800 3800
Wire Wire Line
	9800 3800 9650 3800
Connection ~ 9200 3800
Connection ~ 9200 3400
Wire Wire Line
	9800 3400 9200 3400
$Comp
L Switch:SW_Push USER
U 1 1 62B09D3A
P 9200 3600
F 0 "USER" V 9154 3552 50  0000 R CNN
F 1 "BUTTON 1" V 9245 3552 50  0000 R CNN
F 2 "" H 9200 3800 50  0001 C CNN
F 3 "~" H 9200 3800 50  0001 C CNN
	1    9200 3600
	0    -1   1    0   
$EndComp
Wire Wire Line
	9200 3900 9200 3800
Wire Wire Line
	9350 3800 9200 3800
$Comp
L power:GNDREF #PWR?
U 1 1 62B09D30
P 9200 3900
F 0 "#PWR?" H 9200 3650 50  0001 C CNN
F 1 "GNDREF" H 9205 3727 50  0000 C CNN
F 2 "" H 9200 3900 50  0001 C CNN
F 3 "" H 9200 3900 50  0001 C CNN
	1    9200 3900
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 62B09D2A
P 9500 3800
F 0 "R6" V 9293 3800 50  0000 C CNN
F 1 "100R" V 9384 3800 50  0000 C CNN
F 2 "" V 9430 3800 50  0001 C CNN
F 3 "~" H 9500 3800 50  0001 C CNN
	1    9500 3800
	0    -1   1    0   
$EndComp
Wire Wire Line
	8550 3400 9200 3400
$Comp
L Device:R R?
U 1 1 62B139DA
P 9200 3250
F 0 "R?" H 9270 3296 50  0000 L CNN
F 1 "R" H 9270 3205 50  0000 L CNN
F 2 "" V 9130 3250 50  0001 C CNN
F 3 "~" H 9200 3250 50  0001 C CNN
	1    9200 3250
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 62B141B1
P 9200 3100
F 0 "#PWR?" H 9200 2950 50  0001 C CNN
F 1 "VDD" H 9215 3273 50  0000 C CNN
F 2 "" H 9200 3100 50  0001 C CNN
F 3 "" H 9200 3100 50  0001 C CNN
	1    9200 3100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
